# AFT Übung 2 - Webpack

Dieses Repository enthält ein `package.json` mit allen nötigen NPM Modulen,
die für die Übung mit Webpack benötigt werden. Außerdem sind bereits einige 
Dateien sowie eine Ordnerstruktur vorgegeben. Der restliche Code wird in der
Übung gemeinsam ausimplementiert.

Um alle NPM Module zu installieren einfach folgenden Befehl ausführen:
```
> npm install
```